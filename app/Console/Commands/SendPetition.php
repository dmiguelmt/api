<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class SendPetition extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'request:send {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a simple POST';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $retry = 0;
        $response = null;
        do {
            $response = Http::post($this->argument('url'));
            $retry++;
        } while ($response->status() != 200 && $retry < 20);    
    
        switch ($response->status()) {
            case 200:
                $this->info('success');
                \Log::info("success");
            break;
            default:
                $this->error('Something went wrong, error: '.$response->status());
                \Log::info("error");
            break;
        }
    }
}
